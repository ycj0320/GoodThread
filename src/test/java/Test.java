import com.lp.thread.GoodThreadPool;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class Test {
    public static void main(String[] args) throws InterruptedException {
        Queue<Runnable> que = new ArrayBlockingQueue<Runnable>(5);
        que.add(new TestThread());
        que.add(new TestThread());
        que.add(new TestThread());
        que.add(new TestThread());
        que.add(new TestThread());

        GoodThreadPool goodThreadPool = new GoodThreadPool(7, true, que);
        goodThreadPool.excute();

    }


    public static class TestThread implements Runnable{


        public void run() {
            System.out.println("执行中");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
